package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.logging.Logger;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class IngredientResourceTest extends JerseyTest {
    private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());
    private IngredientDao dao;

    
    @Override
    protected Application configure() {
     BDDFactory.setJdbiForTests();

     return new ApiV1();
  }

  @Before
  public void setEnvUp() {
      dao = BDDFactory.buildDao(IngredientDao.class);
      dao.createTable();
  }

  @After
  public void tearEnvDown() throws Exception {
     dao.dropTable();
  }

  @Test
  public void testGetExistingIngredient() {
	  LOGGER.info("testGetExistingIngredient");
	  
      Ingredient ingredient = new Ingredient();
      ingredient.setName("Chorizo");

      long id = dao.insert(ingredient.getName());
      ingredient.setId(id);

      Response response = target("/ingredients/" + id).request().get();

      assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

      Ingredient result = Ingredient.fromDto(response.readEntity(IngredientDto.class));
      assertEquals(ingredient, result);
  }
    

    
  @Test
  public void testGetNotExistingPizza() {
	  LOGGER.info("testGetNotExistingPizza");
    Response response = target("/pizzas/125").request().get();
    assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
  }

  
  @Test
  public void testCreateIngredient() {
	  LOGGER.info("testCreateIngredient");
      IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();
      ingredientCreateDto.setName("Chorizo");

      Response response = target("/ingredients")
              .request()
              .post(Entity.json(ingredientCreateDto));

      // On vérifie le code de status à 201
      assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

      IngredientDto returnedEntity = response.readEntity(IngredientDto.class);

      // On vérifie que le champ d'entête Location correspond à
      // l'URI de la nouvelle entité
      assertEquals(target("/ingredients/" +
  		returnedEntity.getId()).getUri(), response.getLocation());
  	
  	// On vérifie que le nom correspond
      assertEquals(returnedEntity.getName(), ingredientCreateDto.getName());
  }
  	
  @Test
  public void testCreateSameIngredient() {
	  LOGGER.info("testCreateSameIngredient");
      IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();
      ingredientCreateDto.setName("Chorizo");
      dao.insert(ingredientCreateDto.getName());

      Response response = target("/ingredients")
              .request()
              .post(Entity.json(ingredientCreateDto));

      assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
  }

  @Test
  public void testCreateIngredientWithoutName() {
	  LOGGER.info("testCreateIngredientWithoutName");
      IngredientCreateDto ingredientCreateDto = new IngredientCreateDto();

      Response response = target("/ingredients")
              .request()
              .post(Entity.json(ingredientCreateDto));

      assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
  }
  
  @Test
  public void testDeleteExistingIngredient() {
	  LOGGER.info("testDeleteExistingIngredient");
      Ingredient ingredient = new Ingredient();
      ingredient.setName("Chorizo");
      long id = dao.insert(ingredient.getName());
      ingredient.setId(id);

      Response response = target("/ingredients/" + id).request().delete();

      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

      Ingredient result = dao.findById(id);
  	assertEquals(result, null);
  }

  @Test
  public void testDeleteNotExistingIngredient() {
	  LOGGER.info("testDeleteNotExistingIngredient");
      Response response = target("/ingredients/125").request().delete();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
  	response.getStatus());
  }


  @Test
  public void testGetIngredientName() {
	  LOGGER.info("testGetIngredientName");
      Ingredient ingredient = new Ingredient();
      ingredient.setName("Chorizo");
      long id = dao.insert(ingredient.getName());

      Response response = target("ingredients/" + id + "/name").request().get();

      assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

      assertEquals("Chorizo", response.readEntity(String.class));
  }

  @Test
  public void testGetNotExistingIngredientName() {
	  LOGGER.info("testGetNotExistingIngredientName");
      Response response = target("ingredients/125/name").request().get();

      assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
  }
}


