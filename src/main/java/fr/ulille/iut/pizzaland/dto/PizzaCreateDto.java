package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private String name;
	private List<Ingredient> composition;
	
	public PizzaCreateDto() {}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public List<Ingredient> getIngredient() {
		return composition;
	}

	public void setIngredient(List<Ingredient> composition) {
		this.composition = composition;
	}
}
