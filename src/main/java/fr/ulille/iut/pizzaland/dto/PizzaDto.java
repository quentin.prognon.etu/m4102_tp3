package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {

	private long id;
	private String name;
	private List<Ingredient> composition;

	public PizzaDto() {
		this.composition = new ArrayList<>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<Ingredient> getComposition() {
		return composition;
	}

	public void setComposition(List<Ingredient> composition) {
		this.composition = composition;
	}



}
