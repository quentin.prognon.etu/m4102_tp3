package fr.ulille.iut.pizzaland.resources;

import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;

import java.net.URI;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

@Path("/pizza")
public class PizzaResource {


	private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());
	private PizzaDao pizza;

	public PizzaResource() {
		pizza = BDDFactory.buildDao(PizzaDao.class);
		pizza.createTable();
	}

	@Context
	public UriInfo uriInfo;


	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("IngredientResource:getAll");

		List<PizzaDto> l = pizza.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		return l;
	}

	@GET
	@Path("{id}")
	public PizzaDto getOnePizza(@PathParam("id") long id) {
		LOGGER.info("getOnePizza(" + id + ")");
		try {
			Pizza pizzas = pizza.findById(id);
			return Pizza.toDto(pizzas);
		}
		catch ( Exception e ) {
			// Cette exception générera une réponse avec une erreur 404
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
    @POST
    public Response createPizza(PizzaCreateDto pizzaCreateDto) {
        Pizza existing = pizza.findByName(pizzaCreateDto.getName());
        if ( existing != null ) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }
        
        try {
            Pizza pizzas = Pizza.fromPizzaCreateDto(pizzaCreateDto);
            long id = pizza.insert(pizzas.getName());
            pizzas.setId(id);
            PizzaDto pizzaDto = Pizza.toDto(pizzas);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(pizzaDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }
    
    @DELETE
    @Path("{id}")
    public Response deletePizza(@PathParam("id") long id) {
    	if ( pizza.findById(id) == null ) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}

        pizza.remove(id);

        return Response.status(Response.Status.ACCEPTED).build();
    }
    
    @GET
    @Path("{id}/name")
    public String getPizzaName(@PathParam("id") long id) {
        Pizza pizzas = pizza.findById(id);
    	if ( pizzas == null ) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
            
    	return pizzas.getName();
    }

}
