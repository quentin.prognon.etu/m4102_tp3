package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> composition = new ArrayList<Ingredient>();
	
	public Pizza(){}
	
	public Pizza(long id , String name , List<Ingredient> composition) {
		this.id = id;
		this.name = name;
		this.composition = composition;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getComposition() {
		return composition;
	}

	public void setComposition(List<Ingredient> composition) {
		this.composition = composition;
	}
	
	public static PizzaDto toDto(Pizza p) {
		PizzaDto pizza = new PizzaDto();
		pizza.setComposition(p.getComposition());
		pizza.setId(p.id);
		pizza.setName(p.getName());
		return pizza;
	}
	
	public static Pizza fromDto(PizzaDto p) {
		Pizza pizza = new Pizza();
		pizza.setComposition(p.getComposition());
		pizza.setId(p.getId());
		pizza.setName(p.getName());
		return pizza;
		
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (composition == null) {
			if (other.composition != null)
				return false;
		} else if (!composition.equals(other.composition))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + ", composition=" + composition + "]";
	}
	
	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());
		dto.setIngredient(pizza.getComposition());
		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());
		pizza.setComposition(dto.getIngredient());
		return pizza;
	}

}
